import java.io.File;
import java.io.BufferedReader;

data class Claim(val id: Int = 0, val rowIndex: Int = 0, val colIndex: Int = 0, val rowSize: Int = 0, val colSize: Int = 0)

fun main(args: Array<String>) {
    var fabric = Array(1000, {IntArray(1000)})
    var rowIndex: Int;
    var colIndex: Int;
    val sharedInches: HashMap<String, Int> = hashMapOf();
    var reader = readFileAsLinesUsingBufferedReader("03.txt");
    var line : String?  = reader.readLine().trim();
    var uniqueClaims : MutableList<Int> = IntRange(1, 1000).toMutableList();

    while (line != null) {
        val claim = parseInputString(line);
        rowIndex = claim.rowIndex;
        var sharedCount = 0;
        while (rowIndex < claim.rowSize) {
            colIndex = claim.colIndex;
            while (colIndex < claim.colSize) {
                if (fabric[rowIndex][colIndex] == 0) {
                    fabric[rowIndex][colIndex] = claim.id;
                } else {
                    uniqueClaims.remove(fabric[rowIndex][colIndex]);
                    uniqueClaims.remove(claim.id);
                    sharedInches.put("$rowIndex,$colIndex", claim.id);
                    sharedCount++;
                }
                colIndex++;
            }
            rowIndex++;
        }
        line  = reader.readLine();
    }
    println("Shared Inches : " + sharedInches.count());
    println("Unique claim : " + uniqueClaims.get(0));
}

fun parseInputString(claim: String) : Claim {
    var splitByAt = claim.split("@");
    var splitByColon = splitByAt.get(1).trim().split(":");
    var size = splitByColon.get(1).trim().split("x");
    var rowCol = splitByColon.get(0).trim().split(",");
    return Claim(splitByAt[0].trim().drop(1).toInt(),
                rowCol[1].trim().toInt(),
                rowCol[0].trim().toInt(),
            size[1].trim().toInt() + rowCol[1].trim().toInt(),
            size[0].trim().toInt() + rowCol[0].trim().toInt());
}

fun readFileAsLinesUsingBufferedReader(fileName: String): BufferedReader
        = File(fileName).bufferedReader()